import static org.junit.jupiter.api.Assertions.*;

import java.awt.RenderingHints.Key;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

class SearchTest {

	@Test
	void testSearchByKeyword() throws InterruptedException {
//		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
//		WebDriver browser = new ChromeDriver();
		
//		System.setProperty("webdriver.gecko.driver", "D:\\geckodriver.exe");
//		WebDriver browser = new FirefoxDriver();
		
//		System.setProperty("webdriver.gecko.driver", "D:\\IEDriverServer.exe");
//		WebDriver browser = new FirefoxDriver();
		
		
		
		WebDriver browser = null;
		String bws = "firefox";
		
		//condition 
		if(bws.equalsIgnoreCase("chrome")) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		browser = new ChromeDriver();
		}
		else if (bws.equalsIgnoreCase("firefox")) {
		System.setProperty("webdriver.gecko.driver", "D:\\geckodriver.exe");
		browser = new FirefoxDriver();
		} else  {
		System.setProperty("webdriver.ie.driver", "D:\\IEDriverServer.exe");
		browser = new InternetExplorerDriver();
		
		
		browser.get("https://www.google.com");
		browser.findElement(By.name("q")).sendKeys("NPRU");
		browser.findElement(By.name("q")).sendKeys(Keys.ENTER);
		
//		browser.findElement(By.name("btnK")).click();
//		System.out.println("Page title : "+browser.getTitle());
		
		String pageTitle = browser.getTitle();
		
		String result = browser.findElement(By.xpath("/html/body/div[7]/div/div[10]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[1]/div/div[2]/h2")).getText();
		
		assertEquals("NPRU - ค้นหาด้วย Google", pageTitle);
		assertEquals("มหาวิทยาลัยราชภัฏนครปฐม",result);
		System.out.println("ตรงเฉย");
		
//		System.out.println("ไม่ตรงไอสัส");
		Thread.sleep(5000);
		browser.close();
	}

	}
	}
